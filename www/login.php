<?php

session_start();
require( __DIR__.'/vendor/facebook/php-sdk-v4/src/Facebook/autoload.php');

$fb = new Facebook\Facebook([
  'app_id' => '177280618950684',
  'app_secret' => '8f4ded53661fba41e4174cc88dc38f19',
  'default_graph_version' => 'v2.5',
]);

$helper = $fb->getRedirectLoginHelper();
$permissions = ['email', 'user_likes','publish_actions']; // optional
$loginUrl = $helper->getLoginUrl('http://104.236.221.178/hotspot/login-callback.php',$permissions);


    $_SESSION["mac"]=$_POST['mac'];
    $_SESSION["ip"]=$_POST['ip'];
    $_SESSION["username"]=$_POST['username'];
    $_SESSION["linklogin"]=$_POST['link-login'];
    $_SESSION["linkorig"]=$_POST['link-orig'];
    $_SESSION["error"]=$_POST['error'];
    $_SESSION["trial"]=$_POST['trial'];
    $_SESSION["loginby"]=$_POST['login-by'];
    $_SESSION["chapid"]=$_POST['chap-id'];
    $_SESSION["chapchallenge"]=$_POST['chap-challenge'];
    $_SESSION["linkloginonly"]=$_POST['link-login-only'];
    $_SESSION["linkorigesc"]=$_POST['link-orig-esc'];
    $_SESSION["macesc"]=$_POST['mac-esc'];
    $_SESSION["identity"]=$_POST['identity'];
    $_SESSION["bytesinnice"]=$_POST['bytes-in-nice'];
    $_SESSION["bytesoutnice"]=$_POST['bytes-out-nice'];
    $_SESSION["sessiontimeleft"]=$_POST['session-time-left'];
    $_SESSION["uptime"]=$_POST['uptime'];
    $_SESSION["refreshtimeout"]=$_POST['refresh-timeout'];   
    $_SESSION["linkstatus"]=$_POST['link-status'];  
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Mikrotik Hotspot | Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
</head>
<body>

<div id="wrap">
    <div class="navbar navbar-inverse navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#"><?php echo $identity; ?></a>
            </div>
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active"><a href="http://104.236.221.178/hotspot">Login</a></li>
                    <li><a href="http://104.236.221.178/hotspot/status">Status</a></li>
                    <li><a href="http://104.236.221.178/hotspot/logout?erase-cookie=true">Logout</a></li>
                </ul>
            </div>
        </div>
    </div>

    <div id="bottom-menu">
        <div class="container">
            <div class="row">
                <div class="col-md-2 mylogo">
                    <a href="http://agratitudesign.blogspot.com/" ref="index.html"><img src="img/agratitudesignlogo2.png" alt="logo"></a>
                </div> 
                <div class="col-xs-10 textlogo">
                    <h1>Agratitudesign Hotspot</h1>
                </div>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="col-md-6 col-sm-12">
        
            <div class="row">
                <?php if($error) : ?>
                    <div class="alert alert-danger"><?php echo $error; ?></div>
                <?php endif; ?>

                <div class="alert alert-info">Please log on to use the hotspot service.</div>
                <?php if($trial == 'yes') : ?> 
                    <div class="alert alert-info">
                        Free trial available, <a href="<?php echo $linkloginonly; ?>?dst=<?php echo $linkorigesc; ?>&amp;username=T-<?php echo $macesc; ?>">click here</a>.
                    </div>
                <?php endif; ?>
            </div>

            <div class="row">            
                <div class="panel panel-default">
                    
                    <div class="panel-body">

                        <form id="loginForm" class="form-horizontal" role="form" action="<?php echo $linkloginonly; ?>" method="post">
                            <input type="hidden" name="dst" value="<?php echo $linkorig; ?>"/>
                            <input type="hidden" name="popup" value="true"/>

                            <div class="form-group">
                                <label for="inputLogin" class="col-sm-2 control-label">Login</label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control input-lg" id="inputLogin" name="username"
                                           placeholder="Login" autofocus required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword" class="col-sm-2 control-label">Password</label>

                                <div class="col-sm-10">
                                    <input type="password" class="form-control input-lg" id="inputPassword" name="password"
                                           placeholder="Password" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button type="submit" class="btn btn-primary btn-block btn-lg">OK</button>
                                </div>
                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-6 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-body">

                    <div class="card hovercard">
                        <div class="cardheader">
                        </div>
                        <div class="avatar">
                        <img alt="" src="img/agratitudesignlogo.png">
                        </div>
                        <div class="info">
                        <div class="title">
                        <a href="http://agratitudesign.blogspot.com/">Agratitudesign HighSpeed Hotspot</a>
                        </div>
                        <div class="desc">Website Hotspot Interface For Free</div>
                        <div class="desc">created by <a target="_blank" href="http://agratitudesign.blogspot.com/" title="Agratitudesign Hotspot Templates">agratitudesign.blogspot.com</a></div>
                        <div class="desc">supported by <a target="_blank" href="http://wiswaweb.com/" title="Agratitudesign Hotspot Templates">wiswaweb.com</a></div>
                        </div>
                        <div class="bottom">
                        <a class="btn btn-primary btn-twitter btn-sm" href="https://twitter.com/agratitudesign"><i class="fa fa-twitter"></i></a>
                        <a class="btn btn-danger btn-sm" rel="publisher" href="https://plus.google.com/+KetutAgusSuardika"><i class="fa fa-google-plus"></i></a>
                        <a class="btn btn-primary btn-sm" rel="publisher" href="https://www.facebook.com/pages/Agratitudesign/451131721572773"><i class="fa fa-facebook"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<div id="footer">
    <div class="container">
        <p class="text-muted">Powered by <a href="http://agratitudesign.blogspot.com/">Agratitudesign</a></p>
    </div>
</div>


<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>


<?php if($chapid) : ?> 
<script type="text/javascript" src="js/md5.js"></script>
<script type="text/javascript">
<!--
    function doLogin() {
    <?php if(strlen($chapid) < 1) echo "return true;\n"; ?>
    document.sendin.username.value = document.login.username.value;
    document.sendin.password.value = hexMD5('<?php echo $chapid; ?>' + document.login.password.value + '<?php echo $chapchallenge; ?>');
    document.sendin.submit();
    return false;
    }
//-->
</script>
<?php endif; ?>

<script type="text/javascript">
  document.login.username.focus();
</script>

</body>
</html>
